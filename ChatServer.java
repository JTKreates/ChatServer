// ChatServer

import java.net.*;
import java.io.*;

public class ChatServer
{
    ServerSocket serverSocket;
    Socket clientSocket;
    PrintWriter outputStream;
    BufferedReader inputStream;

    public void run(int port)
    {
        try
        {
            serverSocket = new ServerSocket(5000);
            clientSocket = serverSocket.accept();

            outputStream = new PrintWriter(clientSocket.getOutputStream());
            inputStream = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            String message = null;

            while((message = inputStream.readLine()) != null)
            {
                System.out.println(message);
            }

        } catch (IOException ioe)
        {
            ioe.printStackTrace();
        }
    }

    public static void main(String[] args)
    {
        ChatServer server = new ChatServer();
        server.run(5000);
    }
}